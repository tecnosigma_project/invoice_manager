# README

## Sumário

- [Sobre](#sobre)
  - [Dependências](#dependências)
- [Configuração Local](#configuração-local)
  - [Usando o docker](#usando-o-docker)

## Sobre

O Invoice Manager é um sistema web que permite ao usuário MEI (Micro Empreendedor Individual) o gerenciamento de seus recebíveis (Notas Fiscais).

Nele, o usuário poderá:

- Gerenciar Notas Fiscais;
- Gerenciar Despesas;
- Gerenciar limites de faaturamento (MEI);
- Receber notificações de alertas;
- Receber informativos mensais;
- Visualização de gráficos;

[De volta ao topo](#sumário)

### Dependências

O sistema possui as seguintes dependências:

- MySQL;
- Redis;
- Sidekiq

[De volta ao topo](#sumário)

## Configuração local

### Usando o docker

#### Requer
```
* Docker version (20.10.14)
* Docker compose (1.25.0)
```
#### 1. Clone do projeto
```
git clone git@bitbucket.org:tecnosigma_project/invoice_manager.git
```

#### 2. Build!
```
make build
```

#### 3 rails server

``` shell
make run
```

##### 3.1 Acesso ao container

``` shell
make bash
```

##### 3.2 Rails console

``` shell
make console
```

##### 3.3 rodar a aplicação inteira

Caso você queira rodar a aplicação inteira, isso inclui workers do sidekiq será necessário usar o docker-compose

``` shell
docker-compose up
```

### 4. Testes
```
make tests - roda todos os testes
```

### 5. Qualidade
```
make rubocop
```
```
make rubycritic
```
### 6. Vulnerabilidades
```
make brakeman
```

### Tipos de testes do projeto
 - RSpec: Utilizamos Rspec para todos os testes.
 - Rubocop: Verificação da qualidade do código.
 - Rubycritic:  Verificação da qualidade do código.
 - Simplecov: Verifica a cobertura de testes do projeto.
