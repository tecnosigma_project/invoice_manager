# frozen_string_literal: true

class String
  def to_currency
    gsub(',', '').gsub('.', '').to_f / 100
  end
end
