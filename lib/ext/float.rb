# frozen_string_literal: true

class Float
  def to_currency
    Money.from_cents(self * 100, 'BRL').format
  end
end
