# frozen_string_literal: true

# module responsible by manage application messages
module Messages
  def self.errors
    {
      required_field: I18n.t('messages.errors.required_field'),
      invalid_format: I18n.t('messages.errors.invalid_format'),
      invalid_date: I18n.t('messages.errors.invalid_date'),
      duplicated_value: I18n.t('messages.errors.duplicated_value'),
      invalid_value: I18n.t('messages.errors.invalid_value')
    }
  end
end
