# frozen_string_literal: true

module Regex
  class << self
    def cnpj
      %r{\A^([0-9]{3}\.?[0-9]{3}\.?[0-9]{3}-?[0-9]{2}|[0-9]{2}\.?[0-9]{3}\.?[0-9]{3}/?[0-9]{4}-?[0-9]{2})$\z}
    end
  end
end
