Rails.application.routes.draw do
  require 'sidekiq/web'
  require 'sidekiq/cron/web'

  mount Sidekiq::Web => '/sidekiq'

  resources :users

  root 'admin/dashboard#index'

  devise_for :user,
             path: '',
             path_names: {
               sign_in: 'login',
               sign_out: 'logout'
             }

  scope module: 'admin', path: 'admin', as: 'admin' do
    get    '/dashboard/empresas',            to: 'dashboard/companies#list'
    get    '/dashboard/empresas/novo',       to: 'dashboard/companies#new'
    get    '/dashboard/empresas/:id/editar', to: 'dashboard/companies#edit', as: 'editar_empresa'
    post   '/dashboard/empresas/create',     to: 'dashboard/companies#create'
    put    '/dashboard/empresas/update',     to: 'dashboard/companies#update'
    delete '/dashboard/empresas/:id/delete', to: 'dashboard/companies#delete', as: 'deletar_empresa'

    get    '/dashboard/categorias',            to: 'dashboard/categories#list'
    get    '/dashboard/categorias/novo',       to: 'dashboard/categories#new'
    get    '/dashboard/categorias/:id/editar', to: 'dashboard/categories#edit', as: 'editar_categoria'
    post   '/dashboard/categorias/create',     to: 'dashboard/categories#create'
    put    '/dashboard/categorias/update',     to: 'dashboard/categories#update'
    delete '/dashboard/categorias/:id/delete', to: 'dashboard/categories#delete', as: 'deletar_categoria'

    get    '/dashboard/notas_fiscais',            to: 'dashboard/invoices#list'
    get    '/dashboard/notas_fiscais/novo',       to: 'dashboard/invoices#new'
    get    '/dashboard/notas_fiscais/:id/editar', to: 'dashboard/invoices#edit', as: 'editar_nota_fiscal'
    post   '/dashboard/notas_fiscais/create',     to: 'dashboard/invoices#create'
    put    '/dashboard/notas_fiscais/update',     to: 'dashboard/invoices#update'
    delete '/dashboard/notas_fiscais/:id/delete', to: 'dashboard/invoices#delete', as: 'deletar_nota_fiscal'

    get    '/dashboard/despesas',            to: 'dashboard/expenditures#list'
    get    '/dashboard/despesas/novo',       to: 'dashboard/expenditures#new'
    get    '/dashboard/despesas/:id/editar', to: 'dashboard/expenditures#edit', as: 'editar_despesas'
    post   '/dashboard/despesas/create',     to: 'dashboard/expenditures#create'
    put    '/dashboard/despesas/update',     to: 'dashboard/expenditures#update'
    delete '/dashboard/despesas/:id/delete', to: 'dashboard/expenditures#delete', as: 'deletar_despesas'

    get 'dashboard/historico_de_lancamentos', to: 'dashboard/release_histories#list', as: 'historico_de_lancamentos'

    get '/dashboard/limite_faturamento',        to: 'dashboard/invoicing_limits#index'
    put '/dashboard/limite_faturamento/update', to: 'dashboard/invoicing_limits#update'

    get '/dashboard/notificacoes/:id/editar',  to: 'dashboard/notifications#edit', as: 'editar_notificacoes'
    put '/dashboard/notificacoes/update_auth', to: 'dashboard/notifications#update_auth'
  end
end
