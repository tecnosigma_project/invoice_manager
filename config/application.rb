require_relative 'boot'

require 'rails/all'

Bundler.require(*Rails.groups)

module InvoiceManager
  class Application < Rails::Application
    config.load_defaults 5.2

    config.i18n.default_locale = 'pt-BR'

    config.autoload_paths += %W[
      #{config.root}/lib
      #{config.root}/app/presenters
    ]

    config.active_record.observers = :invoice_observer

    # Extensions
    require 'ext/string'
    require 'ext/float'

    config.active_job.queue_adapter = :sidekiq

    # Redis
    config.cache_store = :redis_cache_store, {
      url: ENV.fetch('REDIS_URL', 'redis://redis:6379/0'),
      namespace: 'invoice_manager:cache',
      expires_in: 3000
    }

    # Notifications
    unless Rails.env.test?
      config.action_mailer.delivery_method = :smtp

      config.action_mailer.smtp_settings = {
        address: ENV['EMAIL_ADDRESS'],
        port: 587,
        domain: ENV['EMAIL_DOMAIN'],
        authentication: 'plain',
        user_name: ENV['EMAIL_ACCOUNT'],
        password: ENV['EMAIL_PASSWORD']
      }
    end
  end
end
