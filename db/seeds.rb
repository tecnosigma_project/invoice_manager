# Create invoicing with defalut value
Invoicing.create(maximum_limit: 81000)

# Create users

user = User.create(email: 'tecnooxossi@gmail.com', password: 'inicial1234')
Notification.create(user: user)

unless Rails.env.production?
  for _ in 1..50
    FactoryBot.create(:company, %i(activated deactivated).sample)
  end

  for _ in 1..50
    FactoryBot.create(:category, %i(activated archived).sample)
  end

  companies = Company.activated

  for _ in 1..200
    FactoryBot.create(:invoice, company: companies.sample)
  end

  for _ in 1..200 do
    categories = Category.activated

    FactoryBot.create(:expenditure, category: categories.sample, company: companies.sample)
  end

  expenditure = Expenditure.first
  expenditure.update!(payment_date: 1.year.ago)

  expenditure = Expenditure.last
  expenditure.update!(payment_date: 1.year.ago)

  invoice = Invoice.first
  invoice.update!(receipt_date: 1.year.after)

  invoice = Invoice.last
  invoice.update!(receipt_date: 1.year.ago)
end


