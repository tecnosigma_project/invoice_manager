class CreateInvoices < ActiveRecord::Migration[5.2]
  def change
    create_table :invoices do |t|
      t.string :number
      t.float :amount
      t.string :description
      t.string :competence_month
      t.datetime :receipt_date

      t.references :company, index: true

      t.timestamps
    end
  end
end
