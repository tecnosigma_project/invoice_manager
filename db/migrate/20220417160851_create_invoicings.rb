class CreateInvoicings < ActiveRecord::Migration[5.2]
  def change
    create_table :invoicings do |t|
      t.float :maximum_limit
    end
  end
end
