class CreateExpenditures < ActiveRecord::Migration[5.2]
  def change
    create_table :expenditures do |t|
      t.string :name
      t.float :amount
      t.datetime :payment_date
      t.datetime :competence_date

      t.references :category, index: true
      t.references :company, index: true

      t.timestamps
    end
  end
end
