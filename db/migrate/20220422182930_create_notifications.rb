class CreateNotifications < ActiveRecord::Migration[5.2]
  def change
    create_table :notifications do |t|
      t.boolean :email, default: true
      t.boolean :sms, default: true

      t.references :user, index: true

      t.timestamps
    end
  end
end
