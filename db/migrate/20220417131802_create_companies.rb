class CreateCompanies < ActiveRecord::Migration[5.2]
  def change
    create_table :companies do |t|
      t.string :name
      t.string :corporate_name
      t.string :cnpj
      t.integer :status, default: 1

      t.timestamps
    end
  end
end
