# frozen_string_literal: true

require 'rails_helper'

RSpec.describe BillingInformationsJob, type: :job do
  describe '.perform' do
    it 'enqueues a job' do
      user = FactoryBot.create(:user)
      FactoryBot.create(:notification, user: user)

      allow(Notifications::Billing::Informations)
        .to receive_message_chain(:availability, :deliver_now!) { true }

      described_class.perform_now
    end
  end
end
