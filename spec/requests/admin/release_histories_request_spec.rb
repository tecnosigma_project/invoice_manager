# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Admin::Dashboard::ReleaseHistoriesController', type: 'request' do
  include ControllerMacros

  before(:each) { login_user }

  describe 'GET actions' do
    describe '#list' do
      it 'renders companies page' do
        get '/admin/dashboard/historico_de_lancamentos?year=all'

        expect(response).to render_template(:list)
      end
    end
  end
end
