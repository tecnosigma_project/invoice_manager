# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Admin::Dashboard::InvoicesController', type: 'request' do
  include ControllerMacros

  before(:each) { login_user }

  describe 'GET actions' do
    describe '#list' do
      it 'renders invoices page' do
        get '/admin/dashboard/notas_fiscais'

        expect(response).to render_template(:list)
      end
    end

    describe '#edit' do
      it 'renders invoice page' do
        user = FactoryBot.create(:user)
        FactoryBot.create(:notification, user: user)
        FactoryBot.create(:invoicing, maximum_limit: 81_000.0)

        company = FactoryBot.create(:company)
        invoice = FactoryBot.create(:invoice, company: company)

        get "/admin/dashboard/notas_fiscais/#{invoice.id}/editar"

        expect(response).to render_template(:edit)
      end
    end

    describe '#new' do
      it 'renders new invoice page' do
        get '/admin/dashboard/notas_fiscais/novo'

        expect(response).to render_template(:new)
      end
    end
  end

  describe 'PUT actions' do
    describe '#update' do
      context 'when pass valid params' do
        it 'updates invoice data' do
          user = FactoryBot.create(:user)
          FactoryBot.create(:notification, user: user)
          FactoryBot.create(:invoicing, maximum_limit: 81_000.0)

          company = FactoryBot.create(:company)
          invoice = FactoryBot.create(:invoice, number: '100/2030', company: company)

          new_number = '100/2029'

          put '/admin/dashboard/notas_fiscais/update',
              params: { invoice: { id: invoice.id,
                                   number: new_number,
                                   description: invoice.description,
                                   competence_month: invoice.competence_month,
                                   receipt_date: invoice.receipt_date,
                                   amount: invoice.amount,
                                   company: company.id } }

          result = Invoice.find(invoice.id).number

          expect(result).to eq(new_number)
        end

        it 'shows success message' do
          user = FactoryBot.create(:user)
          FactoryBot.create(:notification, user: user)
          FactoryBot.create(:invoicing, maximum_limit: 81_000.0)

          company = FactoryBot.create(:company)
          invoice = FactoryBot.create(:invoice, number: '100/2030', company: company)

          new_number = '100/2029'

          put '/admin/dashboard/notas_fiscais/update',
              params: { invoice: { id: invoice.id,
                                   number: new_number,
                                   description: invoice.description,
                                   competence_month: invoice.competence_month,
                                   receipt_date: invoice.receipt_date,
                                   amount: invoice.amount,
                                   company: company.id } }

          expect(flash[:notice]).to eq('Dados atualizados com sucesso!')
        end

        it 'redirects to invoice page' do
          user = FactoryBot.create(:user)
          FactoryBot.create(:notification, user: user)
          FactoryBot.create(:invoicing, maximum_limit: 81_000.0)

          company = FactoryBot.create(:company)
          invoice = FactoryBot.create(:invoice, number: '100/2030', company: company)

          new_number = '100/2029'

          put '/admin/dashboard/notas_fiscais/update',
              params: { invoice: { id: invoice.id,
                                   number: new_number,
                                   description: invoice.description,
                                   competence_month: invoice.competence_month,
                                   receipt_date: invoice.receipt_date,
                                   amount: invoice.amount,
                                   company: company.id } }

          expect(response).to redirect_to(admin_dashboard_notas_fiscais_path)
        end
      end

      context 'when pass invalid params' do
        it 'no updates invoice data' do
          user = FactoryBot.create(:user)
          FactoryBot.create(:notification, user: user)
          FactoryBot.create(:invoicing, maximum_limit: 81_000.0)

          company = FactoryBot.create(:company)
          invoice = FactoryBot.create(:invoice, number: '100/2030', company: company)

          put '/admin/dashboard/notas_fiscais/update',
              params: { invoice: { id: invoice.id,
                                   number: '',
                                   description: invoice.description,
                                   competence_month: invoice.competence_month,
                                   receipt_date: invoice.receipt_date,
                                   amount: invoice.amount,
                                   company: company.id } }

          result = Invoice.find(invoice.id).number

          expect(result).to eq(invoice.number)
        end

        it 'shows error message' do
          user = FactoryBot.create(:user)
          FactoryBot.create(:notification, user: user)
          FactoryBot.create(:invoicing, maximum_limit: 81_000.0)

          company = FactoryBot.create(:company)
          invoice = FactoryBot.create(:invoice, number: '100/2030', company: company)

          put '/admin/dashboard/notas_fiscais/update',
              params: { invoice: { id: invoice.id,
                                   number: '',
                                   description: invoice.description,
                                   competence_month: invoice.competence_month,
                                   receipt_date: invoice.receipt_date,
                                   amount: invoice.amount,
                                   company: company.id } }

          expect(flash[:alert]).to eq('Falha ao atualizar dados!')
        end

        it 'redirects to invoice page' do
          user = FactoryBot.create(:user)
          FactoryBot.create(:notification, user: user)
          FactoryBot.create(:invoicing, maximum_limit: 81_000.0)

          company = FactoryBot.create(:company)
          invoice = FactoryBot.create(:invoice, number: '100/2030', company: company)

          put '/admin/dashboard/notas_fiscais/update',
              params: { invoice: { id: invoice.id,
                                   number: '',
                                   description: invoice.description,
                                   competence_month: invoice.competence_month,
                                   receipt_date: invoice.receipt_date,
                                   amount: invoice.amount,
                                   company: company.id } }

          expect(response).to redirect_to(admin_dashboard_notas_fiscais_path)
        end
      end
    end
  end

  describe 'DELETE actions' do
    describe '#delete' do
      it 'deletes invoice' do
        user = FactoryBot.create(:user)
        FactoryBot.create(:notification, user: user)
        FactoryBot.create(:invoicing, maximum_limit: 81_000.0)

        company = FactoryBot.create(:company, :activated)
        invoice = FactoryBot.create(:invoice, company: company)

        delete "/admin/dashboard/notas_fiscais/#{invoice.id}/delete"

        result = Invoice.find_by_number(invoice.number)

        expect(result).to be_nil
      end

      it 'shows success message' do
        user = FactoryBot.create(:user)
        FactoryBot.create(:notification, user: user)
        FactoryBot.create(:invoicing, maximum_limit: 81_000.0)

        company = FactoryBot.create(:company, :activated)
        invoice = FactoryBot.create(:invoice, company: company)

        delete "/admin/dashboard/notas_fiscais/#{invoice.id}/delete"

        expect(flash[:notice]).to eq('Dados removidos com sucesso!')
      end

      it 'redirects to invoice page' do
        user = FactoryBot.create(:user)
        FactoryBot.create(:notification, user: user)
        FactoryBot.create(:invoicing, maximum_limit: 81_000.0)

        company = FactoryBot.create(:company, :activated)
        invoice = FactoryBot.create(:invoice, company: company)

        delete "/admin/dashboard/notas_fiscais/#{invoice.id}/delete"

        expect(response).to redirect_to(admin_dashboard_notas_fiscais_path)
      end
    end
  end

  describe 'POST actions' do
    describe '#create' do
      context 'when pass valid params' do
        it 'creates new invoice' do
          user = FactoryBot.create(:user)
          FactoryBot.create(:notification, user: user)
          FactoryBot.create(:invoicing, maximum_limit: 81_000.0)

          company = FactoryBot.create(:company, :activated)
          invoice_params = FactoryBot.attributes_for(:invoice)
          invoice_params.merge!({ 'company' => company.id })

          post '/admin/dashboard/notas_fiscais/create',
               params: { invoice: invoice_params }

          result = Invoice.find_by_number(invoice_params[:number])

          expect(result).to be_present
        end

        it 'shows success message' do
          user = FactoryBot.create(:user)
          FactoryBot.create(:notification, user: user)
          FactoryBot.create(:invoicing, maximum_limit: 81_000.0)

          company = FactoryBot.create(:company, :activated)
          invoice_params = FactoryBot.attributes_for(:invoice)
          invoice_params.merge!({ 'company' => company.id })

          post '/admin/dashboard/notas_fiscais/create',
               params: { invoice: invoice_params }

          expect(flash[:notice]).to eq('Dados gravados com sucesso!')
        end

        it 'redirects to invoice page' do
          company = FactoryBot.create(:company, :activated)
          invoice_params = FactoryBot.attributes_for(:invoice)
          invoice_params.merge!({ 'company' => company.id })

          post '/admin/dashboard/notas_fiscais/create',
               params: { invoice: invoice_params }

          expect(response).to redirect_to(admin_dashboard_notas_fiscais_path)
        end
      end

      context 'when pass invalid params' do
        it 'no creates new invoice' do
          invoice_params = FactoryBot.attributes_for(:invoice)
          invoice_params.merge!({ 'company' => '' })

          post '/admin/dashboard/notas_fiscais/create',
               params: { invoice: invoice_params }

          result = Invoice.find_by_number(invoice_params[:number])

          expect(result).to be_nil
        end

        it 'shows error message' do
          invoice_params = FactoryBot.attributes_for(:invoice)
          invoice_params.merge!({ 'company' => '' })

          post '/admin/dashboard/notas_fiscais/create',
               params: { invoice: invoice_params }

          expect(flash[:alert]).to eq('Falha ao gravar dados!')
        end

        it 'redirects to invoice page' do
          invoice_params = FactoryBot.attributes_for(:invoice)
          invoice_params.merge!({ 'company' => '' })

          post '/admin/dashboard/notas_fiscais/create',
               params: { invoice: invoice_params }

          expect(response).to redirect_to(admin_dashboard_notas_fiscais_path)
        end
      end
    end
  end
end
