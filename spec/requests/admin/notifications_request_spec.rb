# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Admin::Dashboard::NotificationsController', type: 'request' do
  include ControllerMacros

  before(:each) { login_user }

  describe 'GET actions' do
    describe '#edt' do
      it 'renders user notificaitons page' do
        user = User.last
        FactoryBot.create(:notification, user: user)

        get "/admin/dashboard/notificacoes/#{user.id}/editar"

        expect(response).to render_template(:edit)
      end
    end
  end

  describe '#update' do
    context 'when pass valid params' do
      context 'and do want receive notifications only by email' do
        it 'updates receipt of notifications' do
          notification = FactoryBot.create(:notification, user: User.last)
          notification.update!(email: true, sms: true)

          put '/admin/dashboard/notificacoes/update_auth',
              params: { notification: { email: '1', sms: '0' } }

          result = User.last.notification

          expect(result.email).to eq(true)
          expect(result.sms).to eq(false)
        end

        it 'shows success message' do
          notification = FactoryBot.create(:notification, user: User.last)
          notification.update!(email: true, sms: true)

          put '/admin/dashboard/notificacoes/update_auth',
              params: { notification: { email: '1', sms: '0' } }

          expect(flash[:notice]).to eq('Dados atualizados com sucesso!')
        end

        it 'redirects to notifications page' do
          user = User.last
          notification = FactoryBot.create(:notification, user: user)
          notification.update!(email: true, sms: true)

          put '/admin/dashboard/notificacoes/update_auth',
              params: { notification: { email: '1', sms: '0' } }

          expect(response).to redirect_to(admin_editar_notificacoes_path(user.id))
        end
      end

      context 'and do want receive notifications only by SMS' do
        it 'updates receipt of notifications' do
          notification = FactoryBot.create(:notification, user: User.last)
          notification.update!(email: true, sms: true)

          put '/admin/dashboard/notificacoes/update_auth',
              params: { notification: { email: '0', sms: '1' } }

          result = User.last.notification

          expect(result.email).to eq(false)
          expect(result.sms).to eq(true)
        end

        it 'shows success message' do
          notification = FactoryBot.create(:notification, user: User.last)
          notification.update!(email: true, sms: true)

          put '/admin/dashboard/notificacoes/update_auth',
              params: { notification: { email: '0', sms: '1' } }

          expect(flash[:notice]).to eq('Dados atualizados com sucesso!')
        end

        it 'redirects to notifications page' do
          user = User.last
          notification = FactoryBot.create(:notification, user: user)
          notification.update!(email: true, sms: true)

          put '/admin/dashboard/notificacoes/update_auth',
              params: { notification: { email: '0', sms: '1' } }

          expect(response).to redirect_to(admin_editar_notificacoes_path(user.id))
        end
      end

      context 'and do want receive notifications only by email and by SMS' do
        it 'updates receipt of notifications' do
          notification = FactoryBot.create(:notification, user: User.last)
          notification.update!(email: true, sms: true)

          put '/admin/dashboard/notificacoes/update_auth',
              params: { notification: { email: '1', sms: '1' } }

          result = User.last.notification

          expect(result.email).to eq(true)
          expect(result.sms).to eq(true)
        end

        it 'shows success message' do
          notification = FactoryBot.create(:notification, user: User.last)
          notification.update!(email: true, sms: true)

          put '/admin/dashboard/notificacoes/update_auth',
              params: { notification: { email: '1', sms: '1' } }

          expect(flash[:notice]).to eq('Dados atualizados com sucesso!')
        end

        it 'redirects to notifications page' do
          user = User.last
          notification = FactoryBot.create(:notification, user: user)
          notification.update!(email: true, sms: true)

          put '/admin/dashboard/notificacoes/update_auth',
              params: { notification: { email: '1', sms: '1' } }

          expect(response).to redirect_to(admin_editar_notificacoes_path(user.id))
        end
      end
    end
  end
end
