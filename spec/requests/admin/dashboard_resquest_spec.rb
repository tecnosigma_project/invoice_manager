# frozen_string_literal: true

# frozen_string_literal: 'true

require 'rails_helper'

RSpec.describe 'Admin::Dashboard', type: 'request' do
  include ControllerMacros

  describe 'GET actions' do
    describe '#index' do
      it 'renders dashboard page' do
        FactoryBot.create(:invoicing, maximum_limit: 81_000.0)

        allow(Invoice).to receive(:totalization) { 30_000.0 }

        login_user

        get '/'

        expect(response).to render_template(:index)
      end
    end
  end
end
