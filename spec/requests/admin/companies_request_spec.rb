# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Admin::Dashboard::CompaniesController', type: 'request' do
  include ControllerMacros

  before(:each) { login_user }

  describe 'GET actions' do
    describe '#list' do
      it 'renders companies page' do
        get '/admin/dashboard/empresas'

        expect(response).to render_template(:list)
      end
    end

    describe '#new' do
      it 'renders new company page' do
        get '/admin/dashboard/empresas/novo'

        expect(response).to render_template(:new)
      end
    end

    describe '#edit' do
      it 'renders company page' do
        company = FactoryBot.create(:company)

        get "/admin/dashboard/empresas/#{company.id}/editar"

        expect(response).to render_template(:edit)
      end
    end
  end

  describe 'DELETE actions' do
    describe '#delete' do
      it 'deletes company' do
        company = FactoryBot.create(:company)

        delete "/admin/dashboard/empresas/#{company.id}/delete"

        result = Company.find_by_name(company.name)

        expect(result).to be_nil
      end

      it 'shows success message' do
        company = FactoryBot.create(:company)

        delete "/admin/dashboard/empresas/#{company.id}/delete"

        expect(flash[:notice]).to eq('Dados removidos com sucesso!')
      end

      it 'redirects to company page' do
        company = FactoryBot.create(:company)

        delete "/admin/dashboard/empresas/#{company.id}/delete"

        expect(response).to redirect_to(admin_dashboard_empresas_path)
      end
    end
  end

  describe 'PUT actions' do
    describe '#update' do
      context 'when pass valid params' do
        it 'updates company data' do
          company = FactoryBot.create(:company, name: 'XPTO S.A.')

          new_name = 'ACME.S.A.'

          put '/admin/dashboard/empresas/update',
              params: { company: { id: company.id, name: new_name } }

          result = Company.find(company.id).name

          expect(result).to eq(new_name)
        end

        it 'shows success message' do
          company = FactoryBot.create(:company, name: 'XPTO S.A.')

          new_name = 'ACME.S.A.'

          put '/admin/dashboard/empresas/update',
              params: { company: { id: company.id, name: new_name } }

          expect(flash[:notice]).to eq('Dados atualizados com sucesso!')
        end

        it 'redirects to company page' do
          company = FactoryBot.create(:company, name: 'XPTO S.A.')

          new_name = 'ACME.S.A.'

          put '/admin/dashboard/empresas/update',
              params: { company: { id: company.id, name: new_name } }

          expect(response).to redirect_to(admin_dashboard_empresas_path)
        end
      end

      context 'when pass invalid params' do
        it 'no updates company data' do
          name = 'XPTO S.A.'
          company = FactoryBot.create(:company, name: name)

          put '/admin/dashboard/empresas/update',
              params: { id: company.id, company: { name: '' } }

          result = Company.find(company.id).name

          expect(result).to eq(name)
        end

        it 'shows error message' do
          company = FactoryBot.create(:company, name: 'XPTO S.A.')

          put '/admin/dashboard/empresas/update',
              params: { id: company.id, company: { name: '' } }

          expect(flash[:alert]).to eq('Falha ao atualizar dados!')
        end

        it 'redirects to company page' do
          company = FactoryBot.create(:company, name: 'XPTO S.A.')

          put '/admin/dashboard/empresas/update',
              params: { id: company.id, company: { name: '' } }

          expect(response).to redirect_to(admin_dashboard_empresas_path)
        end
      end
    end
  end

  describe 'POST actions' do
    describe '#create' do
      context 'when pass valid params' do
        it 'creates new company' do
          company_params = FactoryBot.attributes_for(:company)
          company_params.merge!({ 'status' => 'activated' })

          post '/admin/dashboard/empresas/create',
               params: { company: company_params }

          result = Company.find_by_name(company_params[:name])

          expect(result).to be_present
        end

        it 'shows success message' do
          company_params = FactoryBot.attributes_for(:company)
          company_params.merge!({ 'status' => 'activated' })

          post '/admin/dashboard/empresas/create',
               params: { company: company_params }

          expect(flash[:notice]).to eq('Dados gravados com sucesso!')
        end

        it 'redirects to company page' do
          company_params = FactoryBot.attributes_for(:company)
          company_params.merge!({ 'status' => 'ativado' })

          post '/admin/dashboard/empresas/create',
               params: { company: company_params }

          expect(response).to redirect_to(admin_dashboard_empresas_path)
        end
      end

      context 'when pass invalid params' do
        it 'no creates new company' do
          company_params = FactoryBot.attributes_for(:company)
          company_params.merge!({ 'status' => 'invalid_status' })

          post '/admin/dashboard/empresas/create',
               params: { company: company_params }

          result = Company.find_by_name(company_params[:name])

          expect(result).to be_nil
        end

        it 'shows error message' do
          company_params = FactoryBot.attributes_for(:company)
          company_params.merge!({ 'status' => 'invalid_status' })

          post '/admin/dashboard/empresas/create',
               params: { company: company_params }

          expect(flash[:alert]).to eq('Falha ao gravar dados!')
        end

        it 'redirects to company page' do
          company_params = FactoryBot.attributes_for(:company)
          company_params.merge!({ 'status' => 'invalid_status' })

          post '/admin/dashboard/empresas/create',
               params: { company: company_params }

          expect(response).to redirect_to(admin_dashboard_empresas_path)
        end
      end
    end
  end
end
