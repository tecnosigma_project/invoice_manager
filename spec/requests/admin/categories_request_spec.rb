# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Admin::Dashboard::CategoriesController', type: 'request' do
  include ControllerMacros

  before(:each) { login_user }

  describe 'GET actions' do
    describe '#list' do
      it 'renders companies page' do
        get '/admin/dashboard/categorias'

        expect(response).to render_template(:list)
      end
    end

    describe '#new' do
      it 'renders new category page' do
        get '/admin/dashboard/categorias/novo'

        expect(response).to render_template(:new)
      end
    end

    describe '#edit' do
      it 'renders category page' do
        category = FactoryBot.create(:category)

        get "/admin/dashboard/categorias/#{category.id}/editar"

        expect(response).to render_template(:edit)
      end
    end
  end

  describe 'PUT actions' do
    describe '#update' do
      context 'when pass valid params' do
        it 'updates category data' do
          category = FactoryBot.create(:category, name: 'XPTO')

          new_name = 'ACME'

          put '/admin/dashboard/categorias/update',
              params: { category: { id: category.id, name: new_name } }

          result = Category.find(category.id).name

          expect(result).to eq(new_name)
        end

        it 'shows success message' do
          category = FactoryBot.create(:category, name: 'XPTO')

          new_name = 'ACME'

          put '/admin/dashboard/categorias/update',
              params: { category: { id: category.id, name: new_name } }

          expect(flash[:notice]).to eq('Dados atualizados com sucesso!')
        end

        it 'redirects to category page' do
          category = FactoryBot.create(:category, name: 'XPTO')

          new_name = 'ACME'

          put '/admin/dashboard/categorias/update',
              params: { category: { id: category.id, name: new_name } }

          expect(response).to redirect_to(admin_dashboard_categorias_path)
        end
      end

      context 'when pass invalid params' do
        it 'no updates category data' do
          name = 'XPTO'
          category = FactoryBot.create(:category, name: name)

          put '/admin/dashboard/categorias/update',
              params: { id: category.id, category: { name: '' } }

          result = Category.find(category.id).name

          expect(result).to eq(name)
        end

        it 'shows error message' do
          category = FactoryBot.create(:category, name: 'XPTO')

          put '/admin/dashboard/categorias/update',
              params: { id: category.id, category: { name: '' } }

          expect(flash[:alert]).to eq('Falha ao atualizar dados!')
        end

        it 'redirects to category page' do
          category = FactoryBot.create(:category, name: 'XPTO')

          put '/admin/dashboard/categorias/update',
              params: { id: category.id, category: { name: '' } }

          expect(response).to redirect_to(admin_dashboard_categorias_path)
        end
      end
    end
  end

  describe 'DELETE actions' do
    describe '#delete' do
      it 'deletes category' do
        category = FactoryBot.create(:category)

        delete "/admin/dashboard/categorias/#{category.id}/delete"

        result = Category.find_by_name(category.name)

        expect(result).to be_nil
      end

      it 'shows success message' do
        category = FactoryBot.create(:category)

        delete "/admin/dashboard/categorias/#{category.id}/delete"

        expect(flash[:notice]).to eq('Dados removidos com sucesso!')
      end

      it 'redirects to category page' do
        category = FactoryBot.create(:category)

        delete "/admin/dashboard/categorias/#{category.id}/delete"

        expect(response).to redirect_to(admin_dashboard_categorias_path)
      end
    end
  end

  describe 'POST actions' do
    describe '#create' do
      context 'when pass valid params' do
        it 'creates new category' do
          category_params = FactoryBot.attributes_for(:category)
          category_params.merge!({ 'status' => 'activated' })

          post '/admin/dashboard/categorias/create',
               params: { category: category_params }

          result = Category.find_by_name(category_params[:name])

          expect(result).to be_present
        end

        it 'shows success message' do
          category_params = FactoryBot.attributes_for(:category)
          category_params.merge!({ 'status' => 'activated' })

          post '/admin/dashboard/categorias/create',
               params: { category: category_params }

          expect(flash[:notice]).to eq('Dados gravados com sucesso!')
        end

        it 'redirects to category page' do
          category_params = FactoryBot.attributes_for(:category)
          category_params.merge!({ 'status' => 'ativado' })

          post '/admin/dashboard/categorias/create',
               params: { category: category_params }

          expect(response).to redirect_to(admin_dashboard_categorias_path)
        end
      end

      context 'when pass invalid params' do
        it 'no creates new category' do
          category_params = FactoryBot.attributes_for(:category)
          category_params.merge!({ 'status' => 'invalid_status' })

          post '/admin/dashboard/categorias/create',
               params: { category: category_params }

          result = Company.find_by_name(category_params[:name])

          expect(result).to be_nil
        end

        it 'shows error message' do
          category_params = FactoryBot.attributes_for(:category)
          category_params.merge!({ 'status' => 'invalid_status' })

          post '/admin/dashboard/categorias/create',
               params: { category: category_params }

          expect(flash[:alert]).to eq('Falha ao gravar dados!')
        end

        it 'redirects to category page' do
          category_params = FactoryBot.attributes_for(:category)
          category_params.merge!({ 'status' => 'invalid_status' })

          post '/admin/dashboard/categorias/create',
               params: { category: category_params }

          expect(response).to redirect_to(admin_dashboard_categorias_path)
        end
      end
    end
  end
end
