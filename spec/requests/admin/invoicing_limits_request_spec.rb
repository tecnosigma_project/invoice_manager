# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Admin::Dashboard::InvoicingLimitsController', type: 'request' do
  include ControllerMacros

  before(:each) { login_user }

  describe 'GET actions' do
    describe '#index' do
      it 'renders invoicing limit page' do
        FactoryBot.create(:invoicing, maximum_limit: 81_000.0)

        get '/admin/dashboard/limite_faturamento'

        expect(response).to render_template(:index)
      end
    end

    describe '#update' do
      context 'when pass valid params' do
        it 'updates maximum limit' do
          invoicing = FactoryBot.create(:invoicing, maximum_limit: 81_000.0)

          new_maximum_limit = '200,00'

          put '/admin/dashboard/limite_faturamento/update',
              params: { invoicing_limit: { maximum_limit: new_maximum_limit } }

          result = Invoicing.maximum_limit

          expected_result = 200.0

          expect(result).to eq(expected_result)
        end

        it 'redirects to invoicing limit page' do
          FactoryBot.create(:invoicing, maximum_limit: 10.0)

          new_maximum_limit = 200.0

          put '/admin/dashboard/limite_faturamento/update',
              params: { invoicing_limit: { maximum_limit: new_maximum_limit } }

          expect(response).to redirect_to(admin_dashboard_limite_faturamento_path)
        end

        it 'show success message' do
          FactoryBot.create(:invoicing, maximum_limit: 10.0)

          new_maximum_limit = 200.0

          put '/admin/dashboard/limite_faturamento/update',
              params: { invoicing_limit: { maximum_limit: new_maximum_limit } }

          expect(flash[:notice]).to eq('Dados atualizados com sucesso!')
        end
      end

      context 'when pass invalid params' do
        it 'no updates maximum limit' do
          invoicing = FactoryBot.create(:invoicing, maximum_limit: 10.0)

          new_maximum_limit = -200.0

          put '/admin/dashboard/limite_faturamento/update',
              params: { invoicing_limit: { maximum_limit: new_maximum_limit } }

          result = Invoicing.first.maximum_limit

          expect(result).not_to eq(new_maximum_limit)
        end

        it 'redirects to invoicing limit page' do
          FactoryBot.create(:invoicing, maximum_limit: 10.0)

          new_maximum_limit = -200.0

          put '/admin/dashboard/limite_faturamento/update',
              params: { invoicing_limit: { maximum_limit: new_maximum_limit } }

          expect(response).to redirect_to(admin_dashboard_limite_faturamento_path)
        end

        it 'show error message' do
          FactoryBot.create(:invoicing, maximum_limit: 10.0)

          new_maximum_limit = -200.0

          put '/admin/dashboard/limite_faturamento/update',
              params: { invoicing_limit: { maximum_limit: new_maximum_limit } }

          expect(flash[:alert]).to eq('Falha ao atualizar dados!')
        end
      end
    end
  end
end
