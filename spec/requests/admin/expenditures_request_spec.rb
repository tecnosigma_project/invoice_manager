# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Admin::Dashboard::ExpendituresController', type: 'request' do
  include ControllerMacros

  before(:each) { login_user }

  describe 'GET actions' do
    describe '#list' do
      it 'renders expenditures page' do
        get '/admin/dashboard/despesas'

        expect(response).to render_template(:list)
      end
    end

    describe '#new' do
      it 'renders new expenditure page' do
        get '/admin/dashboard/despesas/novo'

        expect(response).to render_template(:new)
      end
    end

    describe '#edit' do
      it 'renders company page' do
        category = FactoryBot.create(:category)
        company = FactoryBot.create(:company, :activated)
        expenditure = FactoryBot.create(:expenditure, company: company, category: category)

        get "/admin/dashboard/despesas/#{expenditure.id}/editar"

        expect(response).to render_template(:edit)
      end
    end
  end

  describe 'PUT actions' do
    describe '#update' do
      context 'when pass valid params' do
        it 'updates expenditure data' do
          company = FactoryBot.create(:company, :activated)
          category = FactoryBot.create(:category, :activated)
          expenditure = FactoryBot.create(:expenditure, name: 'despesas gerais', company: company, category: category)

          new_name = 'despesas mensais'

          put '/admin/dashboard/despesas/update',
              params: { expenditure: { id: expenditure.id,
                                       name: new_name,
                                       amount: expenditure.amount,
                                       payment_date: expenditure.payment_date,
                                       competence_date: expenditure.competence_date,
                                       category: category.id,
                                       company: company.id } }

          result = Expenditure.find(expenditure.id).name

          expect(result).to eq(new_name)
        end

        it 'shows success message' do
          company = FactoryBot.create(:company, :activated)
          category = FactoryBot.create(:category, :activated)
          expenditure = FactoryBot.create(:expenditure, name: 'despesas gerais', company: company, category: category)

          new_name = 'despesas mensais'

          put '/admin/dashboard/despesas/update',
              params: { expenditure: { id: expenditure.id,
                                       name: new_name,
                                       amount: expenditure.amount,
                                       payment_date: expenditure.payment_date,
                                       competence_date: expenditure.competence_date,
                                       category: category.id,
                                       company: company.id } }

          expect(flash[:notice]).to eq('Dados atualizados com sucesso!')
        end

        it 'redirects to expenditure page' do
          company = FactoryBot.create(:company, :activated)
          category = FactoryBot.create(:category, :activated)
          expenditure = FactoryBot.create(:expenditure, name: 'despesas gerais', company: company, category: category)

          new_name = 'despesas mensais'

          put '/admin/dashboard/despesas/update',
              params: { expenditure: { id: expenditure.id,
                                       name: new_name,
                                       amount: expenditure.amount,
                                       payment_date: expenditure.payment_date,
                                       competence_date: expenditure.competence_date,
                                       category: category.id,
                                       company: company.id } }

          expect(response).to redirect_to(admin_dashboard_despesas_path)
        end
      end

      context 'when pass invalid params' do
        it 'no updates expenditure data' do
          company = FactoryBot.create(:company, :activated)
          category = FactoryBot.create(:category, :activated)
          expenditure = FactoryBot.create(:expenditure, name: 'despesas gerais', company: company, category: category)

          put '/admin/dashboard/despesas/update',
              params: { expenditure: { id: expenditure.id,
                                       name: '',
                                       amount: expenditure.amount,
                                       payment_date: expenditure.payment_date,
                                       competence_date: expenditure.competence_date,
                                       category: category.id,
                                       company: company.id } }

          result = Expenditure.find(expenditure.id).name

          expect(result).to eq(expenditure.name)
        end

        it 'shows error message' do
          company = FactoryBot.create(:company, :activated)
          category = FactoryBot.create(:category, :activated)
          expenditure = FactoryBot.create(:expenditure, name: 'despesas gerais', company: company, category: category)

          put '/admin/dashboard/despesas/update',
              params: { expenditure: { id: expenditure.id,
                                       name: '',
                                       amount: expenditure.amount,
                                       payment_date: expenditure.payment_date,
                                       competence_date: expenditure.competence_date,
                                       category: category.id,
                                       company: company.id } }

          expect(flash[:alert]).to eq('Falha ao atualizar dados!')
        end

        it 'redirects to expenditure page' do
          company = FactoryBot.create(:company, :activated)
          category = FactoryBot.create(:category, :activated)
          expenditure = FactoryBot.create(:expenditure, name: 'despesas gerais', company: company, category: category)

          put '/admin/dashboard/despesas/update',
              params: { expenditure: { id: expenditure.id,
                                       name: '',
                                       amount: expenditure.amount,
                                       payment_date: expenditure.payment_date,
                                       competence_date: expenditure.competence_date,
                                       category: category.id,
                                       company: company.id } }

          expect(response).to redirect_to(admin_dashboard_despesas_path)
        end
      end
    end
  end

  describe 'DELETE actions' do
    describe '#delete' do
      it 'deletes expenditure' do
        company = FactoryBot.create(:company, :activated)
        category = FactoryBot.create(:category)
        expenditure = FactoryBot.create(:expenditure, company: company, category: category)

        delete "/admin/dashboard/despesas/#{expenditure.id}/delete"

        result = Invoice.find_by_number(expenditure.name)

        expect(result).to be_nil
      end

      it 'shows success message' do
        company = FactoryBot.create(:company, :activated)
        category = FactoryBot.create(:category)
        expenditure = FactoryBot.create(:expenditure, company: company, category: category)

        delete "/admin/dashboard/despesas/#{expenditure.id}/delete"

        expect(flash[:notice]).to eq('Dados removidos com sucesso!')
      end

      it 'redirects to expenditure page' do
        company = FactoryBot.create(:company, :activated)
        category = FactoryBot.create(:category)
        expenditure = FactoryBot.create(:expenditure, company: company, category: category)

        delete "/admin/dashboard/despesas/#{expenditure.id}/delete"

        expect(response).to redirect_to(admin_dashboard_despesas_path)
      end
    end
  end

  describe 'POST actions' do
    describe '#create' do
      context 'when pass valid params' do
        it 'creates new expenditure' do
          category = FactoryBot.create(:category)
          company = FactoryBot.create(:company, :activated)
          expenditure_params = FactoryBot.attributes_for(:expenditure)
          expenditure_params.merge!({ 'company' => company.id, 'category' => category.id })

          post '/admin/dashboard/despesas/create',
               params: { expenditure: expenditure_params }

          result = Expenditure.find_by_name(expenditure_params[:name])

          expect(result).to be_present
        end

        it 'shows success message' do
          category = FactoryBot.create(:category)
          company = FactoryBot.create(:company, :activated)
          expenditure_params = FactoryBot.attributes_for(:expenditure)
          expenditure_params.merge!({ 'company' => company.id, 'category' => category.id })

          post '/admin/dashboard/despesas/create',
               params: { expenditure: expenditure_params }

          expect(flash[:notice]).to eq('Dados gravados com sucesso!')
        end

        it 'redirects to expenditure page' do
          category = FactoryBot.create(:category)
          company = FactoryBot.create(:company, :activated)
          expenditure_params = FactoryBot.attributes_for(:expenditure)
          expenditure_params.merge!({ 'company' => company.id, 'category' => category.id })

          post '/admin/dashboard/despesas/create',
               params: { expenditure: expenditure_params }

          expect(response).to redirect_to(admin_dashboard_despesas_path)
        end
      end

      context 'when pass invalid params' do
        it 'no creates new expenditure' do
          category = FactoryBot.create(:category)
          expenditure_params = FactoryBot.attributes_for(:expenditure)
          expenditure_params.merge!({ 'company' => '', 'category' => category.id })

          post '/admin/dashboard/despesas/create',
               params: { expenditure: expenditure_params }

          result = Expenditure.find_by_name(expenditure_params[:name])

          expect(result).to be_nil
        end

        it 'shows error message' do
          category = FactoryBot.create(:category)
          expenditure_params = FactoryBot.attributes_for(:expenditure)
          expenditure_params.merge!({ 'company' => '', 'category' => category.id })

          post '/admin/dashboard/despesas/create',
               params: { expenditure: expenditure_params }

          expect(flash[:alert]).to eq('Falha ao gravar dados!')
        end

        it 'redirects to expenditure page' do
          category = FactoryBot.create(:category)
          expenditure_params = FactoryBot.attributes_for(:expenditure)
          expenditure_params.merge!({ 'company' => '', 'category' => category.id })

          post '/admin/dashboard/despesas/create',
               params: { expenditure: expenditure_params }

          expect(response).to redirect_to(admin_dashboard_despesas_path)
        end
      end
    end
  end
end
