# frozen_string_literal:true

require 'rails_helper'

RSpec.describe Regex do
  describe '.cnpj' do
    it 'checks valid CNPJ' do
      cnpj = '12.345.677/0001-33'

      expect(described_class.cnpj).to match(cnpj)
    end

    it 'checks invalid CNPJ' do
      cnpj = '12.345.677/00000-11'

      expect(described_class.cnpj).not_to match(cnpj)
    end
  end
end
