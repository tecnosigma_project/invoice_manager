# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Messages do
  describe '.errors' do
    it 'returns hash containing errors messages' do
      expected_result = { required_field: 'Preenchimento de campo obrigatório!',
                          invalid_format: 'Formato inválido!',
                          invalid_date: 'Data inválida!',
                          duplicated_value: 'Valor duplicado!',
                          invalid_value: 'Valor inválido!' }

      expect(described_class.errors).to eq(expected_result)
    end
  end
end
