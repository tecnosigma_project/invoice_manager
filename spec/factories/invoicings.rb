FactoryBot.define do
  factory :invoicing do
    maximum_limit { Faker::Number.decimal(l_digits: 2) }
  end
end
