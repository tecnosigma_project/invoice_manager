FactoryBot.define do
  factory :notification do
    email { false }
    sms { false }
  end
end
