FactoryBot.define do
  factory :company do
    corporate_name { Faker::Company.name }
    name { Faker::Lorem.word.titleize }
    cnpj { Faker::Company.brazilian_company_number(formatted: true) }
    status { 1 }

    trait :activated do
      status { 1 }
    end

    trait :deactivated do
      status { 2 }
    end
  end
end
