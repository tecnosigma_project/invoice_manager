FactoryBot.define do
  factory :category do
    name { Faker::Lorem.word }
    description { Faker::Lorem.sentence }
    status { 1 }

    trait :activated do
      status { 1 }
    end

    trait :archived do
      status { 2 }
    end
  end
end
