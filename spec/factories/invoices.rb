FactoryBot.define do
  factory :invoice do
    amount { Faker::Number.decimal(l_digits: 2) }
    description { Faker::Lorem.sentence }
    competence_month { %w(Janeiro Fevereiro Março Abril).sample }
    receipt_date { Random.rand(30).days.after }
    sequence(:number) { |number| "#{number}/#{Time.now.year}" }
  end
end
