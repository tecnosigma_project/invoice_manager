FactoryBot.define do
  factory :expenditure do
    name { Faker::Lorem.word }
    amount { Faker::Number.decimal(l_digits: 2) }
    payment_date { Random.rand(30).day.after }
    competence_date { ['01/01/2022'.to_time, '01/02/2022'.to_time, '01/03/2022'.to_time, '01/04/2022'.to_time].sample }
  end
end
