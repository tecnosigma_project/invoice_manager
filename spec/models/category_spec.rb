# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Category, type: :model do
  describe 'validates presences' do
    it 'of name' do
      error_message = 'Preenchimento de campo obrigatório!'
      category = FactoryBot.build(:category, name: nil)

      expect(category).to be_invalid
      expect(category.errors.messages[:name]).to include(error_message)
    end

    it 'of description' do
      error_message = 'Preenchimento de campo obrigatório!'
      category = FactoryBot.build(:category, description: nil)

      expect(category).to be_invalid
      expect(category.errors.messages[:description]).to include(error_message)
    end
  end

  describe 'validates relationships' do
    it 'validates relationship between Category and Expenditure (1:N)' do
      category = Category.new

      expect(category).to respond_to(:expenditures)
    end
  end

  describe 'validagets scopes' do
    it 'returns categories list ordered by name' do
      category1 = FactoryBot.create(:category, name: 'XYZ')
      category2 = FactoryBot.create(:category, name: 'ABC')

      result = described_class.sorted_by_name

      expect_result = [category2, category1]

      expect(result).to eq(expect_result)
    end

    it 'returns activated names list' do
      category1 = FactoryBot.create(:category, :activated, name: 'XYZ')
      category2 = FactoryBot.create(:category, :activated, name: 'ABC')
      FactoryBot.create(:category, :archived, name: 'OPQ')

      result = described_class.activated_names_list

      expect_result = [[category2.name, category2.id], [category1.name, category1.id]]

      expect(result).to eq(expect_result)
    end
  end

  describe '#activated?' do
    it 'returns \'true\' when category is activated' do
      category = FactoryBot.create(:category, :activated)

      result = category.activated?

      expect(result).to eq(true)
    end

    it 'returns \'false\' when category is archived' do
      category = FactoryBot.create(:category, :archived)

      result = category.activated?

      expect(result).to eq(false)
    end
  end
end
