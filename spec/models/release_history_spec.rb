# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ReleaseHistory do
  describe '#call' do
    context 'when pass \'all\' as param' do
      it 'returns hash list containing releases histories of all years ordered by event date' do
        user = FactoryBot.create(:user)
        FactoryBot.create(:notification, user: user)

        FactoryBot.create(:invoicing, maximum_limit: 81_000.0)

        company = FactoryBot.create(:company)
        category = FactoryBot.create(:category)
        expenditure = FactoryBot.create(:expenditure, company: company, category: category, payment_date: 3.days.ago)
        invoice = FactoryBot.create(:invoice, company: company, receipt_date: 3.days.after)

        result = described_class.new(year: 'all').call

        expected_result = [{ kind: invoice.class.to_s,
                             reference: invoice.number,
                             amount: invoice.amount,
                             event_date: invoice.receipt_date },
                           { kind: expenditure.class.to_s,
                             reference: expenditure.name,
                             amount: expenditure.amount,
                             event_date: expenditure.payment_date }]

        expect(result).to eq(expected_result)
      end
    end

    context 'when pass valid year as param' do
      it 'returns hash list containing releases histories of chosen year ordered by event date' do
        user = FactoryBot.create(:user)
        FactoryBot.create(:notification, user: user)

        FactoryBot.create(:invoicing, maximum_limit: 81_000.0)

        company = FactoryBot.create(:company)
        category = FactoryBot.create(:category)
        expenditure = FactoryBot.create(:expenditure, company: company, category: category, payment_date: 3.years.ago)
        invoice = FactoryBot.create(:invoice, company: company, receipt_date: 3.days.after)

        result = described_class.new(year: 3.years.ago.year.to_s).call

        expected_result = [{ kind: expenditure.class.to_s,
                             reference: expenditure.name,
                             amount: expenditure.amount,
                             event_date: expenditure.payment_date }]

        expect(result).to eq(expected_result)
      end
    end

    context 'when pass invalid year param' do
      it 'returns empty list' do
        user = FactoryBot.create(:user)
        FactoryBot.create(:notification, user: user)

        FactoryBot.create(:invoicing, maximum_limit: 81_000.0)

        company = FactoryBot.create(:company)
        category = FactoryBot.create(:category)
        expenditure = FactoryBot.create(:expenditure, company: company, category: category, payment_date: 3.days.ago)
        invoice = FactoryBot.create(:invoice, company: company, receipt_date: 3.days.after)

        result = described_class.new(year: 5000).call

        expect(result).to be_empty
      end
    end

    context 'when no pass param' do
      it 'returns empty list' do
        user = FactoryBot.create(:user)
        FactoryBot.create(:notification, user: user)

        FactoryBot.create(:invoicing, maximum_limit: 81_000.0)

        company = FactoryBot.create(:company)
        category = FactoryBot.create(:category)
        expenditure = FactoryBot.create(:expenditure, company: company, category: category, payment_date: 3.days.ago)
        invoice = FactoryBot.create(:invoice, company: company, receipt_date: 3.days.after)

        result = described_class.new(year: '').call

        expect(result).to be_empty
      end
    end
  end
end
