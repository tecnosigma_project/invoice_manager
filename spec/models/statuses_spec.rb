# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Statuses do
  it 'returns company statuses' do
    result = described_class::COMPANIES

    expected_result = { activated: 1, deactivated: 2 }

    expect(result).to eq(expected_result)
  end

  it 'returns categories statuses' do
    result = described_class::CATEGORIES

    expected_result = { activated: 1, archived: 2 }

    expect(result).to eq(expected_result)
  end

  describe '.companies' do
    it 'returns array of arrays containing status and translated status as elements' do
      result = described_class.companies

      expected_result = [%w[ativado activated], %w[desativado deactivated]]

      expect(result).to eq(expected_result)
    end
  end

  describe '.categories' do
    it 'returns array of arrays containing status and translated status as elements' do
      result = described_class.categories

      expected_result = [%w[ativado activated], %w[arquivado archived]]

      expect(result).to eq(expected_result)
    end
  end
end
