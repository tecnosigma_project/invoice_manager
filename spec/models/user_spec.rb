# frozen_string_literal: true

require 'rails_helper'

RSpec.describe User, type: :model do
  describe 'validates presences' do
    it 'of email' do
      user = FactoryBot.build(:user, email: nil)

      expect(user).to be_invalid
    end

    it 'of password' do
      user = FactoryBot.build(:user, password: nil)

      expect(user).to be_invalid
    end
  end

  describe 'validates relationships' do
    it 'validates relationship between User and Notification (1:1)' do
      user = User.new

      expect(user).to respond_to(:notification)
    end
  end
end
