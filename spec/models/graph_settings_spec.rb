# frozen_string_literal: true

require 'rails_helper'

RSpec.describe GraphSettings do
  it 'validates pie dimensions' do
    result = described_class::PIE_DIMENSIONS

    expected_result = { height: '800px', width: '800px' }

    expect(result).to eq(expected_result)
  end

  it 'validates column dimensions' do
    result = described_class::COLUMN_DIMENSIONS

    expected_result = { height: '800px', width: '800px' }

    expect(result).to eq(expected_result)
  end
end
