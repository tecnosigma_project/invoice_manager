# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Invoice, type: :model do
  describe 'validates presences' do
    it 'of number' do
      error_message = 'Preenchimento de campo obrigatório!'
      company = FactoryBot.create(:company)
      invoice = FactoryBot.build(:invoice, number: nil, company: company)

      expect(invoice).to be_invalid
      expect(invoice.errors.messages[:number]).to include(error_message)
    end

    it 'of amount' do
      error_message = 'Preenchimento de campo obrigatório!'
      company = FactoryBot.create(:company)
      invoice = FactoryBot.build(:invoice, amount: nil, company: company)

      expect(invoice).to be_invalid
      expect(invoice.errors.messages[:amount]).to include(error_message)
    end

    it 'of description' do
      error_message = 'Preenchimento de campo obrigatório!'
      company = FactoryBot.create(:company)
      invoice = FactoryBot.build(:invoice, description: nil, company: company)

      expect(invoice).to be_invalid
      expect(invoice.errors.messages[:description]).to include(error_message)
    end

    it 'of competence_month' do
      error_message = 'Preenchimento de campo obrigatório!'
      company = FactoryBot.create(:company)
      invoice = FactoryBot.build(:invoice, competence_month: nil, company: company)

      expect(invoice).to be_invalid
      expect(invoice.errors.messages[:competence_month]).to include(error_message)
    end

    it 'of receipt date' do
      error_message = 'Preenchimento de campo obrigatório!'
      company = FactoryBot.create(:company)
      invoice = FactoryBot.build(:invoice, receipt_date: nil, company: company)

      expect(invoice).to be_invalid
      expect(invoice.errors.messages[:receipt_date]).to include(error_message)
    end
  end

  describe 'validates relationships' do
    it 'validates relationship between Invoice and Company (N:1)' do
      invoice = Invoice.new

      expect(invoice).to respond_to(:company)
    end
  end

  describe 'validates uniqueness' do
    it 'no validates when invoice number is duplicated' do
      user = FactoryBot.create(:user)
      FactoryBot.create(:notification, user: user)

      FactoryBot.create(:invoicing, maximum_limit: 81_000.0)

      company = FactoryBot.create(:company)
      invoice1 = FactoryBot.create(:invoice, number: '0001/2000', company: company)

      invoice2 = FactoryBot.build(:invoice, number: invoice1.number, company: company)

      expect(invoice2).to be_invalid
    end
  end

  describe 'no validates when amount when' do
    it 'is less than zero' do
      error_message = 'Valor inválido!'
      invoice = FactoryBot.build(:invoice, amount: -10.0)

      expect(invoice).to be_invalid
      expect(invoice.errors.messages[:amount]).to include(error_message)
    end

    it 'is equal than zero' do
      error_message = 'Valor inválido!'
      invoice = FactoryBot.build(:invoice, amount: 0.0)

      expect(invoice).to be_invalid
      expect(invoice.errors.messages[:amount]).to include(error_message)
    end
  end

  describe 'validates scopes' do
    it 'returns totalization of amounts' do
      user = FactoryBot.create(:user)
      FactoryBot.create(:notification, user: user)

      FactoryBot.create(:invoicing, maximum_limit: 81_000.0)

      amount1 = 10.0
      amount2 = 20.0

      company = FactoryBot.create(:company)
      invoice1 = FactoryBot.create(:invoice, number: '0001/2000', amount: amount1, company: company)
      invoice2 = FactoryBot.create(:invoice, number: '0002/2000', amount: amount2, company: company)

      result = described_class.totalization

      expected_result = amount1 + amount2

      expect(result).to eq(expected_result)
    end
  end
end
