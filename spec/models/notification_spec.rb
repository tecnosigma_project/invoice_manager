# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Notification, type: :model do
  describe 'validates relationships' do
    it 'validates relationship between Notificationr and User (1:1)' do
      notification = Notification.new

      expect(notification).to respond_to(:user)
    end
  end
end
