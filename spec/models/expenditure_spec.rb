# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Expenditure, type: :model do
  describe 'validates presences' do
    it 'of name' do
      error_message = 'Preenchimento de campo obrigatório!'
      company = FactoryBot.create(:company)
      expenditure = FactoryBot.build(:expenditure, name: nil, company: company)

      expect(expenditure).to be_invalid
      expect(expenditure.errors.messages[:name]).to include(error_message)
    end

    it 'of amount' do
      error_message = 'Preenchimento de campo obrigatório!'
      company = FactoryBot.create(:company)
      expenditure = FactoryBot.build(:expenditure, amount: nil, company: company)

      expect(expenditure).to be_invalid
      expect(expenditure.errors.messages[:amount]).to include(error_message)
    end

    it 'of payment date' do
      error_message = 'Preenchimento de campo obrigatório!'
      company = FactoryBot.create(:company)
      expenditure = FactoryBot.build(:expenditure, payment_date: nil, company: company)

      expect(expenditure).to be_invalid
      expect(expenditure.errors.messages[:payment_date]).to include(error_message)
    end

    it 'of competence date' do
      error_message = 'Preenchimento de campo obrigatório!'
      company = FactoryBot.create(:company)
      expenditure = FactoryBot.build(:expenditure, competence_date: nil, company: company)

      expect(expenditure).to be_invalid
      expect(expenditure.errors.messages[:competence_date]).to include(error_message)
    end
  end

  describe 'validates relationships' do
    it 'validates relationship between Expenditure and Company (N:1)' do
      expenditure = Expenditure.new

      expect(expenditure).to respond_to(:company)
    end

    it 'validates relationship between Expenditure and Category (N:1)' do
      expenditure = Expenditure.new

      expect(expenditure).to respond_to(:category)
    end
  end

  describe 'validates scopes' do
    it 'returns expenditure list ordered by name' do
      company = FactoryBot.create(:company, :activated)
      category = FactoryBot.create(:category)
      expenditure1 = FactoryBot.create(:expenditure, company: company, category: category, name: 'XYZ')
      expenditure2 = FactoryBot.create(:expenditure, company: company, category: category, name: 'ABC')

      result = described_class.sorted_by_name

      expect_result = [expenditure2, expenditure1]

      expect(result).to eq(expect_result)
    end
  end

  describe 'no validates when amount when' do
    it 'is less than zero' do
      error_message = 'Valor inválido!'
      expenditure = FactoryBot.build(:expenditure, amount: -10.0)

      expect(expenditure).to be_invalid
      expect(expenditure.errors.messages[:amount]).to include(error_message)
    end

    it 'is equal than zero' do
      error_message = 'Valor inválido!'
      expenditure = FactoryBot.build(:expenditure, amount: 0.0)

      expect(expenditure).to be_invalid
      expect(expenditure.errors.messages[:amount]).to include(error_message)
    end
  end

  it 'no validates when competence date is greater than current date' do
    error_message = 'Data inválida!'
    company = FactoryBot.create(:company)
    expenditure = FactoryBot.build(:expenditure, competence_date: 30.days.after, company: company)

    expect(expenditure).to be_invalid
    expect(expenditure.errors.messages[:competence_date]).to include(error_message)
  end
end
