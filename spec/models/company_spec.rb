# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Company, type: :model do
  describe 'validates presences' do
    it 'of corporate name' do
      error_message = 'Preenchimento de campo obrigatório!'
      company = FactoryBot.build(:company, corporate_name: nil)

      expect(company).to be_invalid
      expect(company.errors.messages[:corporate_name]).to include(error_message)
    end

    it 'of name' do
      error_message = 'Preenchimento de campo obrigatório!'
      company = FactoryBot.build(:company, name: nil)

      expect(company).to be_invalid
      expect(company.errors.messages[:name]).to include(error_message)
    end

    it 'of CNPJ' do
      error_message = 'Preenchimento de campo obrigatório!'
      company = FactoryBot.build(:company, cnpj: nil)

      expect(company).to be_invalid
      expect(company.errors.messages[:cnpj]).to include(error_message)
    end
  end

  describe 'validates relationships' do
    it 'validates relationship between Company and Expenditure (1:N)' do
      company = Company.new

      expect(company).to respond_to(:expenditures)
    end

    it 'validates relationship between Company and Invoice (1:N)' do
      company = Company.new

      expect(company).to respond_to(:invoices)
    end
  end

  it 'no validates when pass invalid CNPJ' do
    company = FactoryBot.build(:company, cnpj: '123.456.789')

    expect(company).to be_invalid
  end

  describe 'validates scopes' do
    it 'returns companies list ordered by name' do
      company1 = FactoryBot.create(:company, name: 'XYZ')
      company2 = FactoryBot.create(:company, name: 'ABC')

      result = described_class.sorted_by_name

      expect_result = [company2, company1]

      expect(result).to eq(expect_result)
    end

    it 'returns activated names list' do
      company1 = FactoryBot.create(:company, :activated, name: 'XYZ')
      company2 = FactoryBot.create(:company, :activated, name: 'ABC')
      FactoryBot.create(:company, :deactivated, name: 'OPQ')

      result = described_class.activated_names_list

      expect_result = [[company2.name, company2.id], [company1.name, company1.id]]

      expect(result).to eq(expect_result)
    end
  end

  describe '#activated?' do
    it 'returns \'true\' when company is activated' do
      company = FactoryBot.create(:company, :activated)

      result = company.activated?

      expect(result).to eq(true)
    end

    it 'returns \'false\' when company is deactivated' do
      company = FactoryBot.create(:company, :deactivated)

      result = company.activated?

      expect(result).to eq(false)
    end
  end
end
