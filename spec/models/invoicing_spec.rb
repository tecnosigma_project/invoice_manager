# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Invoicing, type: :model do
  describe 'validates presences' do
    it 'of maximum limit' do
      error_message = 'Preenchimento de campo obrigatório!'
      invoicing = FactoryBot.build(:invoicing, maximum_limit: nil)

      expect(invoicing).to be_invalid
      expect(invoicing.errors.messages[:maximum_limit]).to include(error_message)
    end
  end

  describe 'no validates when maximum limit when' do
    it 'is less than zero' do
      error_message = 'Valor inválido!'
      invoicing = FactoryBot.build(:invoicing, maximum_limit: -10.0)

      expect(invoicing).to be_invalid
      expect(invoicing.errors.messages[:maximum_limit]).to include(error_message)
    end

    it 'is equal than zero' do
      error_message = 'Valor inválido!'
      invoicing = FactoryBot.build(:invoicing, maximum_limit: 0.0)

      expect(invoicing).to be_invalid
      expect(invoicing.errors.messages[:maximum_limit]).to include(error_message)
    end
  end

  describe '.limit_percentage' do
    it 'returns limit percentage' do
      invoicing = FactoryBot.create(:invoicing, maximum_limit: 81_000.0)

      allow(Invoice).to receive(:totalization) { 1000.0 }

      expected_result = 1.23

      result = described_class.limit_percentage

      expect(result).to eq(expected_result)
    end
  end

  describe '.risk_percentage?' do
    it 'returns \'true\' when exist risk percentage' do
      invoicing = FactoryBot.create(:invoicing, maximum_limit: 81_000.0)

      allow(Invoice).to receive(:totalization) { 70_000.0 }

      result = described_class.risk_percentage?

      expect(result).to eq(true)
    end

    it 'returns \'false\' when doesn\'t exist risk percentage' do
      invoicing = FactoryBot.create(:invoicing, maximum_limit: 81_000.0)

      allow(Invoice).to receive(:totalization) { 100.0 }

      result = described_class.risk_percentage?

      expect(result).to eq(false)
    end
  end
end
