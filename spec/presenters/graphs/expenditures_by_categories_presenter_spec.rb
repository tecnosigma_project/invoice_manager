# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Graphs::ExpendituresByCategoriesPresenter do
  describe '.data' do
    it 'returns graph data of expenditures by categories graph' do
      user = FactoryBot.create(:user)
      FactoryBot.create(:notification, user: user)

      category1 = FactoryBot.create(:category, name: 'banana')
      category2 = FactoryBot.create(:category, name: 'uva')
      company = FactoryBot.create(:company)
      expenditure1 = FactoryBot.create(:expenditure, company: company, category: category1)
      expenditure2 = FactoryBot.create(:expenditure, company: company, category: category2)
      expenditure3 = FactoryBot.create(:expenditure, company: company, category: category2)

      result = described_class.data

      expected_result = { 'banana' => expenditure1.amount, 'uva' => (expenditure2.amount + expenditure3.amount).round(2) }

      expect(result).to eq(expected_result)
    end
  end
end
