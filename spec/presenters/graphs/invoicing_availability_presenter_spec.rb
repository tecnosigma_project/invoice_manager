# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Graphs::InvoicingAvailabilityPresenter do
  describe '.data' do
    it 'returns graph data of invoicing availability' do
      allow(Invoicing).to receive(:maximum_limit) { 81_000.0 }
      allow(Invoice).to receive(:totalization) { 75_000.0 }

      result = described_class.data

      expected_result = { 'Disponível para Faturamento' => 6000.0,
                          'Limite de faturamento (MEI)' => 81_000.0 }

      expect(result).to eq(expected_result)
    end
  end
end
