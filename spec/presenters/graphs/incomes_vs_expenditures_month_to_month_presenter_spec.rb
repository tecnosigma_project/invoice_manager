# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Graphs::IncomesVsExpendituresMonthToMonthPresenter do
  describe '.data' do
    it 'returns graph data of invoices vs expenditures month to month' do
      user = FactoryBot.create(:user)
      FactoryBot.create(:notification, user: user)
      invoices = { 'Janeiro' => 3116.45, 'Fevereiro' => 2613.21, 'Março' => 2162.55, 'Abril' => 3173.14 }
      expenditures = { 'Janeiro' => 2948.13, 'Fevereiro' => 2983.04, 'Março' => 2871.95, 'Abril' => 2695.25 }

      allow(Graphs::InvoicesMonthToMonthPresenter).to receive(:data) { invoices }
      allow(Graphs::ExpendituresMonthToMonthPresenter).to receive(:data) { expenditures }

      result = described_class.data

      expected_result = [{ name: 'Receitas',
                           data: { 'Janeiro' => 3116.45, 'Fevereiro' => 2613.21, 'Março' => 2162.55,
                                   'Abril' => 3173.14 },
                           stack: 'stack 1' },
                         { name: 'Despesas',
                           data: { 'Janeiro' => 2948.13, 'Fevereiro' => 2983.04, 'Março' => 2871.95,
                                   'Abril' => 2695.25 },
                           stack: 'stack 2' }]

      expect(result).to eq(expected_result)
    end
  end
end
