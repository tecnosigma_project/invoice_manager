# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Graphs::ExpendituresMonthToMonthPresenter do
  describe '.data' do
    it 'returns graph data of invoices month to month' do
      user = FactoryBot.create(:user)
      FactoryBot.create(:notification, user: user)
      FactoryBot.create(:invoicing, maximum_limit: 81_000.0)
      category = FactoryBot.create(:category)
      company = FactoryBot.create(:company)
      expenditure1 = FactoryBot.create(:expenditure,
                                       company: company,
                                       competence_date: '01/01/2022'.to_time,
                                       amount: 15_000.0,
                                       category: category)
      expenditure2 = FactoryBot.create(:expenditure,
                                       company: company,
                                       competence_date: '24/02/2022'.to_time,
                                       amount: 7500.0,
                                       category: category)
      expenditure3 = FactoryBot.create(:expenditure,
                                       company: company,
                                       competence_date: '28/02/2022'.to_time,
                                       amount: 1500.0,
                                       category: category)

      result = described_class.data

      expected_result = { 'Janeiro' => expenditure1.amount,
                          'Fevereiro' => (expenditure2.amount + expenditure3.amount) }

      expect(result).to eq(expected_result)
    end
  end
end
