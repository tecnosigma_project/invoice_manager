# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Graphs::InvoicesMonthToMonthPresenter do
  describe '.data' do
    it 'returns graph data of invoices month to month' do
      user = FactoryBot.create(:user)
      FactoryBot.create(:notification, user: user)
      FactoryBot.create(:invoicing, maximum_limit: 81_000.0)
      company = FactoryBot.create(:company)
      invoice1 = FactoryBot.create(:invoice, company: company, competence_month: 'Janeiro', amount: 15_000.0)
      invoice2 = FactoryBot.create(:invoice, company: company, competence_month: 'Fevereiro', amount: 7500.0)
      invoice3 = FactoryBot.create(:invoice, company: company, competence_month: 'Fevereiro', amount: 1500.0)

      result = described_class.data

      expected_result = { 'Janeiro' => invoice1.amount,
                          'Fevereiro' => (invoice2.amount + invoice3.amount) }

      expect(result).to eq(expected_result)
    end
  end
end
