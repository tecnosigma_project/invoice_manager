# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  include ApplicationHelper

  describe '#highlight_line' do
    it 'returns style to highlight line when company is deactivated' do
      company = FactoryBot.create(:company, :deactivated)

      result = helper.highlight_line(company)

      expect(result).to eq('color: red')
    end

    it 'no returns style to highlight line when company is activated' do
      company = FactoryBot.create(:company, :activated)

      result = helper.highlight_line(company)

      expect(result).to eq('')
    end
  end

  describe '#formatted_currency' do
    it 'returns formatted currency' do
      amount = 1450

      result = helper.formatted_currency(amount)

      expected_result = 'R$ 1.450,00'

      expect(result).to eq(expected_result)
    end
  end

  describe '#formatted_date' do
    it 'returns formatted date' do
      date = DateTime.now

      result = helper.formatted_date(date)

      expected_result = date.strftime('%d/%m/%Y')

      expect(result).to eq(expected_result)
    end
  end
end
