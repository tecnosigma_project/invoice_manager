# frozen_string_literal: true

# Preview all emails at http://localhost:3000/rails/mailers/notifications/billing_threshold
module Notifications
  class BillingThresholdPreview < ActionMailer::Preview
  end
end
