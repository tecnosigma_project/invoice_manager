# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Notifications::Billing::Informations, type: :mailer do
  describe 'renders the headers' do
    it 'renders to' do
      user = FactoryBot.create(:user)
      invoicing_data = { limit: 'R$81.000,00', current: 'R$40.000,00', available: 'R$41.000,00' }

      allow(Invoicing).to receive(:informations) { invoicing_data }

      result = described_class.availability.to

      expect(result).to eq([user.email])
    end

    it 'renders subject' do
      user = FactoryBot.create(:user)
      invoicing_data = { limit: 'R$81.000,00', current: 'R$40.000,00', available: 'R$41.000,00' }

      allow(Invoicing).to receive(:informations) { invoicing_data }

      result = described_class.availability.subject

      expect(result).to eq('Relatório mensal de limite de faturamento')
    end

    it 'renders from' do
      user = FactoryBot.create(:user)
      invoicing_data = { limit: 'R$81.000,00', current: 'R$40.000,00', available: 'R$41.000,00' }

      allow(Invoicing).to receive(:informations) { invoicing_data }

      result = described_class.availability.from

      expect(result).to eq(['invoicemanagervibbraacm@gmail.com'])
    end

    it 'renders body' do
      user = FactoryBot.create(:user)
      invoicing_data = { limit: 'R$81.000,00', current: 'R$40.000,00', available: 'R$41.000,00' }

      allow(Invoicing).to receive(:informations) { invoicing_data }

      expected_result = "<!DOCTYPE html>\r\n" \
                        "<html>\r\n" \
                        "  <head>\r\n" \
                        "    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\r\n" \
                        "    <style>\r\n" \
                        "      /* Email styles need to be inline */\r\n" \
                        "    </style>\r\n" \
                        "  </head>\r\n" \
                        "\r\n" \
                        "  <body>\r\n" \
                        "    <p>Olá,</p>\r\n" \
                        "<p>Para que você não seja desenquadrado do MEI, oriente-se pela tabela disponibilizada abaixo:</p>\r\n" \
                        "<h2>\r\n" \
                        "<table width='90%'>\r\n" \
                        "  <thead>\r\n" \
                        "    <tr>\r\n" \
                        "      <th style= \"border: 1px solid #ddd\" scope=\"col\">\r\n" \
                        "        Limite MEI\r\n" \
                        "      </th>\r\n" \
                        "      <th style= \"border: 1px solid #ddd\" scope=\"col\">\r\n" \
                        "        Faturamento atual\r\n" \
                        "      </th>\r\n" \
                        "      <th style= \"border: 1px solid #ddd\" scope=\"col\">\r\n" \
                        "        Limite de faturamento disponível\r\n" \
                        "      </th>\r\n" \
                        "    </tr>\r\n" \
                        "  </thead>\r\n" \
                        "  <tbody>\r\n" \
                        "    <tr>\r\n" \
                        "      <td width='30%' style= \"border: 1px solid #ddd; text-align: right\">R$81.000,00</td>\r\n" \
                        "      <td width='30%' style= \"border: 1px solid #ddd; text-align: right\">R$40.000,00</td>\r\n" \
                        "      <td width='30%' style= \"border: 1px solid #ddd; text-align: right\">R$41.000,00</td>\r\n" \
                        "    </tr>\r\n" \
                        "  </tbody>\r\n" \
                        "</table>\r\n" \
                        "</h2>\r\n" \
                        "<br>\r\n" \
                        "<p>Até mais!</p>\r\n" \
                        "<br>\r\n" \
                        "<br>\r\n" \
                        "<p>Equipe Invoice Manager</p>\r\n" \
                        "\r\n" \
                        "  </body>\r\n" \
                        "</html>\r\n"

      result = described_class.availability.body.raw_source

      expect(result).to eq(expected_result)
    end
  end
end
