# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Notifications::Billing::Thresholds, type: :mailer do
  describe 'renders the headers' do
    it 'renders to' do
      FactoryBot.create(:invoicing, maximum_limit: 81_000.0)
      user = FactoryBot.create(:user)

      amount = 3000
      percentage = '87%'

      result = described_class.alert(billing_amount: amount, current_percentage: percentage).to

      expect(result).to eq([user.email])
    end

    it 'renders subject' do
      FactoryBot.create(:invoicing, maximum_limit: 81_000.0)
      FactoryBot.create(:user)

      amount = 3000
      percentage = '87%'

      result = described_class.alert(billing_amount: amount, current_percentage: percentage).subject

      expect(result).to eq('Limite de faturamento anual do MEI')
    end

    it 'renders from' do
      FactoryBot.create(:invoicing, maximum_limit: 81_000.0)
      FactoryBot.create(:user)

      amount = 3000
      percentage = '87%'

      result = described_class.alert(billing_amount: amount, current_percentage: percentage).from

      expect(result).to eq(['invoicemanagervibbraacm@gmail.com'])
    end

    it 'renders body' do
      invoicing = FactoryBot.create(:invoicing, maximum_limit: 81_000.0)
      FactoryBot.create(:user)

      amount = 3000
      percentage = '87%'

      expected_result = "<!DOCTYPE html>\r\n" \
                        "<html>\r\n" \
                        "  <head>\r\n" \
                        "    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\r\n" + "    <style>\r\n" \
                                                                                                              "      /* Email styles need to be inline */\r\n" + "    </style>\r\n" \
                                                                                                                                                                 "  </head>\r\n" \
                                                                                                                                                                 "\r\n" \
                                                                                                                                                                 "  <body>\r\n" + "    <p>Olá,</p>\r\n" \
                                                                                                                                                                                  "<p>Seu limite de faturamento anual do MEI, que é de #{invoicing.maximum_limit.to_currency}, chegou a #{percentage}, o que equivale a:</p>\r\n" \
                                                                                                                                                                                  "<p><h2>3000</h2></p>\r\n" \
                                                                                                                                                                                  "<p>Você está próximo a ser desenquadrado dessa categoria.</p>\r\n" + "<p>Para que isso não ocorra, recomendamos que você acesse o link abaixo e saiba mais informações sobre o que deve fazer.</p>\r\n" \
                                                                                                                                                                                                                                                        "<p>\r\n" \
                                                                                                                                                                                                                                                        "  <h2>\r\n" + "    <a href=\"http://www8.receita.fazenda.gov.br/SimplesNacional/Arquivos/manual/PerguntaoMEI.pdf\" target=\"blank\">\r\n" \
                                                                                                                                                                                                                                                                       "      Perguntas e Respostas - MEI e Simei\r\n" \
                                                                                                                                                                                                                                                                       "    </a>\r\n" + "  </h2>\r\n" \
                                                                                                                                                                                                                                                                                        "</p>\r\n" + "<br>\r\n" \
                                                                                                                                                                                                                                                                                                     "<p>Até mais!</p>\r\n" + "<br>\r\n" \
                                                                                                                                                                                                                                                                                                                              "<br>\r\n" + "<p>Equipe Invoice Manager</p>\r\n" \
                                                                                                                                                                                                                                                                                                                                           "\r\n" \
                                                                                                                                                                                                                                                                                                                                           "  </body>\r\n" \
                                                                                                                                                                                                                                                                                                                                           "</html>\r\n"

      result = described_class
               .alert(billing_amount: amount, current_percentage: percentage)
               .body
               .raw_source

      expect(result).to eq(expected_result)
    end
  end
end
