FROM ruby:2.7.0

RUN apt-get update -qq && apt-get install -y build-essential libpq-dev nodejs graphviz

RUN mkdir /invoice_manager
WORKDIR /invoice_manager

ADD Gemfile /invoice_manager/Gemfile
ADD Gemfile.lock /invoice_manager/Gemfile.lock

RUN bundle install

ADD . /invoice_manager

