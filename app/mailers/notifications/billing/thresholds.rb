# frozen_string_literal: true

module Notifications
  module Billing
    class Thresholds < ApplicationMailer
      def alert(billing_amount:, current_percentage:)
        @billing_threshold = Invoicing.last.maximum_limit.to_currency
        @billing_amount = billing_amount
        @current_percentage = current_percentage

        mail to: User.last.email,
             subject: I18n.t('notifications.billing.thresholds.alert.subject')
      end
    end
  end
end
