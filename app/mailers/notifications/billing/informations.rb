# frozen_string_literal: true

module Notifications
  module Billing
    class Informations < ApplicationMailer
      def availability
        @limit = informations[:limit]
        @current = informations[:current]
        @available = informations[:available]

        mail to: User.last.email,
             subject: I18n.t('notifications.billing.informations.availability.subject')
      end

      private

      def informations
        @informations ||= Invoicing.informations
      end
    end
  end
end
