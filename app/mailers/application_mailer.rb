# frozen_string_literal: true

class ApplicationMailer < ActionMailer::Base
  default from: ENV.fetch('EMAIL_ACCOUNT', nil)
  layout 'mailer'
end
