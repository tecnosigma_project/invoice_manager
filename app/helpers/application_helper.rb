# frozen_string_literal: true

module ApplicationHelper
  def highlight_line(object)
    object.activated? ? ''.html_safe : 'color: red'.html_safe
  end

  def formatted_currency(amount)
    number_to_currency(amount, unit: 'R$ ', separator: ',', delimiter: '.')
  end

  def formatted_date(date)
    date.strftime('%d/%m/%Y')
  end
end
