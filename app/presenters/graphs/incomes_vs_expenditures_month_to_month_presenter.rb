# frozen_string_literal: true

module Graphs
  class IncomesVsExpendituresMonthToMonthPresenter
    def self.data
      invoices = Graphs::InvoicesMonthToMonthPresenter.data
      expenditures = Graphs::ExpendituresMonthToMonthPresenter.data

      [
        { name: 'Receitas', data: invoices, stack: 'stack 1' },
        { name: 'Despesas', data: expenditures, stack: 'stack 2' }
      ]
    end

    def self.convert_month(_hash)
      data
        .transform_keys { |key| (I18n.t('months').find_index(key) + 1) }
        .sort
        .to_h
    end
  end
end
