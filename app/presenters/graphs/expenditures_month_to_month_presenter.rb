# frozen_string_literal: true

module Graphs
  class ExpendituresMonthToMonthPresenter
    def self.data
      expendtures_list
        .group_by { |expenditure| expenditure[:competence_date] }
        .transform_values { |expenditure| expenditure.map { |exp_child| exp_child[:amount] } }
        .transform_values { |amount| amount.reduce(:+).round(2) }
        .sort
        .to_h
        .transform_keys { |key| month_name(key) }
    end

    def self.expendtures_list
      Expenditure.all.map do |expenditure|
        { amount: expenditure.amount,
          competence_date: expenditure.competence_date.strftime('%m').to_i }
      end
    end

    def self.month_name(key)
      I18n.t("month_names.#{Date::MONTHNAMES[key].downcase}")
    end
  end
end
