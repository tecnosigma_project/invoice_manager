# frozen_string_literal: true

module Graphs
  class InvoicingAvailabilityPresenter
    def self.data
      available = (maximum_limit - Invoice.totalization).round(2)

      { I18n.t('admin.dashboard.index.graphs.legends.available_to_billing') => available,
        I18n.t('admin.dashboard.index.graphs.legends.billing_threshold') => maximum_limit }
    end

    def self.maximum_limit
      @maximum_limit ||= Invoicing.maximum_limit
    end

    private_class_method :maximum_limit
  end
end
