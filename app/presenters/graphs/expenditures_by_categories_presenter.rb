# frozen_string_literal: true

module Graphs
  class ExpendituresByCategoriesPresenter
    def self.data
      Category.sorted_by_name.inject({}) do |hash, element|
        total = element.expenditures.pluck(:amount).reduce(:+) || 0.00

        hash.merge!(element.name => total.round(2))
      end
    end
  end
end
