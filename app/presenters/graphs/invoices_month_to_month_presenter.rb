# frozen_string_literal: true

module Graphs
  class InvoicesMonthToMonthPresenter
    def self.data
      Invoice
        .group(:competence_month)
        .sum(:amount)
        .transform_values! { |value| value.round(2) }
        .transform_keys { |key| (I18n.t('months').find_index(key) + 1) }
        .sort
        .to_h
        .transform_keys { |key| month_name(key) }
    end

    def self.month_name(key)
      I18n.t("month_names.#{Date::MONTHNAMES[key].downcase}")
    end
  end
end
