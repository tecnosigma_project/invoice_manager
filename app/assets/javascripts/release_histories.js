$(document).ready(function(){
        $('#filterByYearBtn').click(function(){
                redirectTofilteredPage();
        });
});

function redirectTofilteredPage() {
        const defaultYear = 'all';

        let year;
        let url = window.location.href;

        if($('#releaseYear').val() == '') {
                year = defaultYear;
        } else {
                year = $('#releaseYear').val()
        }

        let urlWithFiltered = `${url.split('=')[0]}=${year}`;

        window.location.replace(urlWithFiltered);
}
