# frozen_string_literal: true

class BillingInformationsJob < ApplicationJob
  queue_as :default

  def perform(*_args)
    return unless User.last.notification.email

    Rails.logger.info('--- Send billing informations notification ---')

    Notifications::Billing::Informations.availability.deliver_now!

    Rails.logger.info('--- billing informations notification sent successfully ---')
  end
end
