# frozen_string_literal: true

module Admin
  # class responbible by manage admin dashboard
  class DashboardController < ApplicationController
    before_action :authenticate_user!

    def index; end

    private

    def delete_obj(obj, route)
      obj.delete

      redirect_to route, notice: t('messages.successes.removed_successfully')
    rescue StandardError => error
      Rails.logger.error("Message: #{error.message} - Backtrace: #{error.backtrace}")

      redirect_to route, alert: t('messages.errors.remove_failed')
    end

    def update_obj(obj, obj_params, route)
      obj.update!(obj_params)

      redirect_to route, notice: t('messages.successes.updated_successfully')
    rescue StandardError => error
      Rails.logger.error("Message: #{error.message} - Backtrace: #{error.backtrace}")

      redirect_to route, alert: t('messages.errors.update_failed')
    end

    def create_obj(obj, obj_params, route)
      new_obj = obj.new(obj_params)
      new_obj.validate!
      new_obj.save!

      redirect_to route, notice: t('messages.successes.created_successfully')
    rescue StandardError => error
      Rails.logger.error("Message: #{error.message} - Backtrace: #{error.backtrace}")

      redirect_to route, alert: t('messages.errors.create_failed')
    end
  end
end
