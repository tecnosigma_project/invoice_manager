# frozen_string_literal: true

module Admin
  module Dashboard
    # class responbible by manage release histories
    class ReleaseHistoriesController < DashboardController
      def list
        @releases = ReleaseHistory.new(year: params['year']).call
      end
    end
  end
end
