# frozen_string_literal: true

module Admin
  module Dashboard
    # class responbible by manage user notifications
    class NotificationsController < DashboardController
      before_action :user, only: %i[edit update_auth]

      def edit; end

      def update_auth
        notification.update!(notification_params)

        redirect_to admin_editar_notificacoes_path(@user.id),
                    notice: t('messages.successes.updated_successfully')
      rescue StandardError => error
        Rails.logger.error("Message: #{error.message} - Backtrace: #{error.backtrace}")

        redirect_to admin_editar_notificacoes_path(@user.id),
                    alert: t('messages.errors.update_failed')
      end

      private

      def user
        @user ||= User.find_by_email(current_user.email)
      end

      def notification
        @user.notification
      end

      def notification_params
        params
          .require(:notification)
          .permit(:email, :sms)
      end
    end
  end
end
