# frozen_string_literal: true

module Admin
  module Dashboard
    # class responbible by manage categories
    class CategoriesController < DashboardController
      before_action :find_category, only: %i[edit update delete]

      def new; end
      def edit; end

      def list
        @categories = Category
                      .sorted_by_name
                      .paginate(page: params[:page], per_page: Category::PER_PAGE)
      end

      def delete
        delete_obj(@category, admin_dashboard_categorias_path)
      end

      def update
        update_obj(@category, category_params, admin_dashboard_categorias_path)
      end

      def create
        create_obj(Category, category_params, admin_dashboard_categorias_path)
      end

      private

      def find_category
        @category = Category.find(params['id'] || params['category']['id'])
      end

      def category_params
        params
          .require(:category)
          .permit(:name, :description, :status)
      end
    end
  end
end
