# frozen_string_literal: true

module Admin
  module Dashboard
    # class responbible by manage companies
    class CompaniesController < DashboardController
      before_action :find_company, only: %i[edit delete update]

      def new; end
      def edit; end

      def list
        @companies = Company
                     .sorted_by_name
                     .paginate(page: params[:page], per_page: Company::PER_PAGE)
      end

      def update
        update_obj(@company, company_params, admin_dashboard_empresas_path)
      end

      def delete
        delete_obj(@company, admin_dashboard_empresas_path)
      end

      def create
        create_obj(Company, company_params, admin_dashboard_empresas_path)
      end

      private

      def find_company
        @company = Company.find(params['id'] || params['company']['id'])
      end

      def company_params
        params
          .require(:company)
          .permit(:name, :corporate_name, :cnpj, :status)
      end
    end
  end
end
