# frozen_string_literal: true

module Admin
  module Dashboard
    # class responbible by manage invoicing limit
    class InvoicingLimitsController < DashboardController
      def index
        @invoicing = Invoicing.last.maximum_limit
      end

      def update
        invoicing = Invoicing.last
        invoicing.update!(invoicing_params)

        redirect_to admin_dashboard_limite_faturamento_path,
                    notice: t('messages.successes.updated_successfully')
      rescue StandardError => error
        Rails.logger.error("Message: #{error.message} - Backtrace: #{error.backtrace}")

        redirect_to admin_dashboard_limite_faturamento_path,
                    alert: t('messages.errors.update_failed')
      end

      private

      def invoicing_params
        params
          .require(:invoicing_limit)
          .permit(:maximum_limit)
          .merge!({ 'maximum_limit' => params['invoicing_limit']['maximum_limit'].to_currency })
      end
    end
  end
end
