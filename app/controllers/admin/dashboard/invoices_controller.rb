# frozen_string_literal: true

module Admin
  module Dashboard
    # class responbible by manage invoices
    class InvoicesController < DashboardController
      before_action :find_invoice, only: %i[edit delete update]

      def new; end
      def edit; end

      def list
        @invoices = Invoice
                    .all
                    .paginate(page: params[:page], per_page: Invoice::PER_PAGE)
      end

      def update
        update_obj(@invoice, invoice_params, admin_dashboard_notas_fiscais_path)
      end

      def create
        create_obj(Invoice, invoice_params, admin_dashboard_notas_fiscais_path)
      end

      def delete
        delete_obj(@invoice, admin_dashboard_notas_fiscais_path)
      end

      private

      def find_invoice
        @invoice = Invoice.find(params['id'] || params['invoice']['id'])
      end

      def company
        Company.find_by_id(params['invoice']['company'])
      end

      def invoice_params
        params
          .require(:invoice)
          .permit(:number, :description, :competence_month, :receipt_date)
          .merge!({ 'company' => company, 'amount' => params['invoice']['amount'].to_currency })
      end
    end
  end
end
