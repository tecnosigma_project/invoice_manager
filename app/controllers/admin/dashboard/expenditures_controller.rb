# frozen_string_literal: true

module Admin
  module Dashboard
    # class responbible by manage expenditures
    class ExpendituresController < DashboardController
      before_action :find_expenditure, only: %i[edit update delete]

      def new; end
      def edit; end

      def list
        @expenditures = Expenditure
                        .sorted_by_name
                        .paginate(page: params[:page], per_page: Expenditure::PER_PAGE)
      end

      def delete
        delete_obj(@expenditure, admin_dashboard_despesas_path)
      end

      def update
        update_obj(@expenditure, expenditure_params, admin_dashboard_despesas_path)
      end

      def create
        create_obj(Expenditure, expenditure_params, admin_dashboard_despesas_path)
      end

      def company
        Company.find_by_id(params['expenditure']['company'])
      end

      def category
        Category.find_by_id(params['expenditure']['category'])
      end

      private

      def find_expenditure
        @expenditure = Expenditure.find(params['id'] || params['expenditure']['id'])
      end

      def expenditure_params
        params
          .require(:expenditure)
          .permit(:name, :payment_date, :competence_date)
          .merge!({ 'company' => company,
                    'category' => category,
                    'amount' => params['expenditure']['amount'].to_currency })
      end
    end
  end
end
