# frozen_string_literal: true

class Company < ApplicationRecord
  validates :name,
            :corporate_name,
            :cnpj,
            presence: { message: Messages.errors[:required_field] }

  validates :cnpj,
            format: { with: Regex.cnpj,
                      message: Messages.errors[:invalid_format] }

  enum status: Statuses::COMPANIES

  has_many :expenditures
  has_many :invoices

  scope :sorted_by_name, -> { order(:name) }
  scope :activated_names_list, -> { sorted_by_name.activated.pluck(:name, :id) }

  PER_PAGE = 15

  def activated?
    status == 'activated'
  end
end
