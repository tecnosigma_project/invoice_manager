# frozen_string_literal: true

class Category < ApplicationRecord
  validates :name,
            :description,
            presence: { message: Messages.errors[:required_field] }

  enum status: Statuses::CATEGORIES

  has_many :expenditures

  PER_PAGE = 15

  scope :sorted_by_name, -> { order(:name) }
  scope :activated_names_list, -> { sorted_by_name.activated.pluck(:name, :id) }

  def activated?
    status == 'activated'
  end
end
