# frozen_string_literal: true

class Invoice < ApplicationRecord
  validates :number,
            :amount,
            :description,
            :competence_month,
            :receipt_date,
            presence: { message: Messages.errors[:required_field] }

  validates :amount,
            numericality: { greater_than: 0, message: Messages.errors[:invalid_value] }

  validates :number,
            uniqueness: { message: Messages.errors[:duplicated_value] }

  belongs_to :company

  PER_PAGE = 15

  scope :totalization, -> { pluck(:amount).reduce(:+) }
end
