# frozen_string_literal: true

class InvoiceObserver < ActiveRecord::Observer
  def after_save(_invoice)
    return unless User.last.notification.email
    return unless Invoicing.risk_percentage?

    totalization = Invoice.totalization.to_currency
    current_percentage = "#{Invoicing.limit_percentage}%"

    send_notification(totalization: totalization, current_percentage: current_percentage)
  end

  private

  def send_notification(totalization:, current_percentage:)
    Notifications::Billing::Thresholds
      .alert(
        billing_amount: totalization,
        current_percentage: current_percentage
      )
      .deliver_now!
  end
end
