# frozen_string_literal: true

class Statuses
  COMPANIES = { activated: 1, deactivated: 2 }.freeze
  CATEGORIES = { activated: 1, archived: 2 }.freeze

  def self.categories
    list(CATEGORIES)
  end

  def self.companies
    list(COMPANIES)
  end

  def self.list(kind)
    kind.inject([]) do |list, element|
      list << [I18n.t("statuses.#{element.first}"), element.first.to_s]
    end
  end

  private_class_method :list
end
