# frozen_string_literal: true

class Invoicing < ApplicationRecord
  validates :maximum_limit,
            presence: { message: Messages.errors[:required_field] }

  validates :maximum_limit,
            numericality: { greater_than: 0, message: Messages.errors[:invalid_value] }

  THRESHOLD_PERCENTAGE = 80

  scope :maximum_limit, -> { last.maximum_limit }

  def self.limit_percentage
    (Invoice.totalization * 100 / maximum_limit).round(2)
  end

  def self.risk_percentage?
    limit_percentage >= THRESHOLD_PERCENTAGE
  end

  def self.informations
    { limit: maximum_limit.to_currency,
      current: Invoice.totalization.to_currency,
      available: (maximum_limit - Invoice.totalization).to_currency }
  end
end
