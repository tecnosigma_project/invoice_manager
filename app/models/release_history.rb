# frozen_string_literal: true

class ReleaseHistory
  attr_reader :year

  def initialize(year: 'all')
    @year = year
  end

  def call
    (releases(Expenditure) + releases(Invoice))
      .sort { |first, last| last[:event_date] <=> first[:event_date] }
  end

  private

  def releases(klass)
    klass.all.map do |release|
      if year == 'all' || year.to_i == event_date(release).year
        { kind: klass.to_s, reference: reference(release),
          amount: release.amount, event_date: event_date(release) }
      end
    end.compact
  end

  def reference(release)
    release.try(:name) || release.try(:number)
  end

  def event_date(release)
    release.try(:payment_date) || release.try(:receipt_date)
  end
end
