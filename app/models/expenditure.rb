# frozen_string_literal: true

class Expenditure < ApplicationRecord
  validates :name,
            :amount,
            :payment_date,
            :competence_date,
            presence: { message: Messages.errors[:required_field] }

  validates :amount,
            numericality: { greater_than: 0, message: Messages.errors[:invalid_value] }

  validate :check_competence_date

  belongs_to :category
  belongs_to :company

  PER_PAGE = 15

  scope :sorted_by_name, -> { order(:name) }

  def check_competence_date
    return unless competence_date

    errors.add(:competence_date, Messages.errors[:invalid_date]) if competence_date > Time.now
  end
end
