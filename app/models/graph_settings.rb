# frozen_string_literal: true

class GraphSettings
  PIE_DIMENSIONS = { width: '800px', height: '800px' }.freeze
  COLUMN_DIMENSIONS = { width: '800px', height: '800px' }.freeze
end
